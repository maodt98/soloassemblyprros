#define _CRT_SECURE_NO_DEPRECATE
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

//-- Constantes
#define TAMANIO_MAX 10000    //Tamanio maximo del archivo
#define NUMERO_CODIGOS 26      //Maximo de valores distintos en el archivo

//-- Definicion de la estructura para el archivo de entrada y el comprimido
typedef struct archivo
{
	int tamanio;
	unsigned char *informacion;
} Archivo;

//-- Arreglos que contendran el codigo de compresion
// arreglo que contiene el codigo. Cada byte contiene un codigo de compresion

unsigned char codigoCompresion[NUMERO_CODIGOS];
// arreglo con la longitud. Cada byte tiene la longitud en bits del codigo de compresion correspondiente
// en el arreglo de arriba
int longitudCodigo[NUMERO_CODIGOS];



//-- Prototipos de las funciones
int readFile(Archivo * archivo, char *);
int readFileCode(unsigned char[], char *);
void writeFile(int, Archivo * archivoCodificado, char *);
int codificar(Archivo * archivo, Archivo * archivoCodificado);
void agregarAlArreglo(unsigned char[], unsigned char, int, int, int);
void uploadCode(unsigned char[], char *);


//-- Funciones

// Esta funcion se encarga de abrir un archivo y leerlo en el vector datos.
// Retorna el n mero de bytes leidos.
// No hay que completar nada en esta funcion.
int readFile( Archivo * archivo, char * nombreArchivo )
{
	FILE *file;
	int n;
	int x;

	if (!(file = fopen(nombreArchivo, "rb"))) {
		printf("No se puede abrir el archivo: %s\n", nombreArchivo);
		exit(EXIT_FAILURE);
	}
	fseek(file, 0, SEEK_END);
	n = ftell(file);
	fseek(file, 0, SEEK_SET);

	archivo -> tamanio = n;
	archivo -> informacion = (unsigned char *) calloc (n, sizeof (unsigned char));

	for ( x = 0; x < n; x++) {
			fread (&archivo -> informacion [x], sizeof(unsigned char ), 1, file);
		}

	fclose(file);

	return n;
}

// Esta funcion se encarga de abrir un archivo de codigo
// No hay que completar nada en esta funcion.
int readFileCode(unsigned char datos[], char *nombreArchivo)
{
	FILE *file;
	int n;

	if (!(file = fopen(nombreArchivo, "rb"))) {
		printf("No se puede abrir el archivo: %s\n", nombreArchivo);
		exit(EXIT_FAILURE);
	}
	fseek(file, 0, SEEK_END);
	n = ftell(file);
	fseek(file, 0, SEEK_SET);
	if (n > TAMANIO_MAX) {
		printf("Archivo demasiado grande: %s\n", nombreArchivo);
		exit(EXIT_FAILURE);
	}

	fread(datos, 1, n, file);

	fclose(file);

	return n;
}

// Esta funcion se encarga de abrir el archivo que contiene el codigo de compresion.
// Hace uso de la funcion readFile
// Guarda el codigo de compresion en el arreglo de estructuras llamado codigo
void uploadCode(unsigned char codigo[], char *nombreArchivoCodigo)
{
	unsigned char archivoCodigo[TAMANIO_MAX];
	//en la primera mitad guardamos el codigo de compresion, en la segunda mitad, en cada byte,
	//guardamos la lognitud en bits de cada traduccion

	int tamanioCodigoTotal = readFileCode(archivoCodigo, nombreArchivoCodigo); // esto deberia ser 52
	int tamanioCodigo = tamanioCodigoTotal / 2; // deberia ser 26
	int i;

	printf("El tamanio del codigo es %d \n", tamanioCodigoTotal);

	//guardamos el codigo y su longitud en los arreglos de arriba
	for ( i = 0; i < tamanioCodigo; i++) {
		codigo[i]   = archivoCodigo[i];
	}

	for ( i = tamanioCodigo; i < tamanioCodigoTotal; i++) {
		longitudCodigo[i - tamanioCodigo] = (int)archivoCodigo[i];
	}
}

// Esta funcion se encarga de escribir un archivo a partir del vector datos.
// El numero de bytes que se deben escribir viene en el parametro n.
// No hay que completar nada en esta funcion.
void writeFile(int n, Archivo * archivoCodificado, char *nombreArchivo)
{
	FILE *file;
	if (!(file = fopen(nombreArchivo, "wb"))) {
		printf("No se puede abrir el archivo: %s\n", nombreArchivo);
		exit(EXIT_FAILURE);
	}

	fwrite((archivoCodificado->informacion), 1, n, file);

	fclose(file);
}


// Esta funcion se encarga de codificar cada uno de los valores que se encuentran en la estructura archivo y asignar los nuevos valores a la estructura archivocodificado.
// Deben desarrollar esta funcion en su totalidad.
int codificar(Archivo * archivo, Archivo * archivocodificado)
{
	int longitud;
	int ultimoBit = 0;
	unsigned char codigoa;
	int tamBytes = 0;
	int byt;
	int index;
	int numLetra;
	int j;
	int h;
	int n = archivo->tamanio;
	unsigned char * datos = archivo->informacion;
	unsigned char * codificados = archivocodificado->informacion;
	int copy = 0;


        
     	__asm
		{
			mov eax, 0
			for1:
			cmp eax, n
				jge salto1
				mov ebx, datos[eax]
				sub ebx, 65
				mov numLetra, ebx; numLetra = datos[index] - 65
				mov ecx, numLetra;
				mov ebx, longitudCodigo[ecx]
				mov longitud, ebx; longitud = longitudCodigo[numLetra]
				mov ebx, longitud
				add copy, ebx; copy += longitud
				inc eax
				jmp for1
				salto1 :
			mov index, eax
				mov ecx, 0
				for2 :
				cmp ecx, n
				jge salto2
				mov eax, datos[eax]
				sub eax, 65
				mov numLetra, eax; numLetra = datos[byte] - 65
				mov eax, 0
				mov edx, numLetra
				mov ah, codigoCompresion[edx]
				mov codigoa, ah; codigoa = codigoCompresion[numLetra]
				mov eax, longitudCodigo[edx]
				mov longitud, eax; longitud = longitudCodigo[numLetra]
				mov eax, copy
				add eax, 7
				mov ebx, 8
				div ebx; (copy + 7) / 8
				push eax
				push ultimoBit
				push longitud
				push codigoa
				push codificados
				call agregarAlArreglo; agregarAlArreglo recurcion
				mov eax, longitud
				add ultimoBit, eax; ultimoBit += longitud
				inc ecx
				jmp for2
				salto2 :
			    mov byt, ecx

				mov eax, copy
				add eax, 7
				mov ebx, 8
				div ebx; (copy + 7) / 8
				mov archivocodificado[0], eax
				mov eax, ultimoBit
		}

		printf("Longitud %i\n", longitud);
		printf("ultimoBit %i\n ", ultimoBit);
		printf("codigoa %u\n ", codigoa);
		printf("tamBytes %i\n ", tamBytes);
		printf("byte %i\n ", byt);
		printf("index %i\n ", index);
		printf("numLetra %i\n ", numLetra);
		printf("n %i\n ", n);
		printf("codificados-datos %u\n ", codificados - datos);
		printf("copy %i\n ", copy);

		return ultimoBit;



}

// Esta funcion recibe como parametros el vector de datos codificados,
// el codigo que se debe insertar, la longitud de este ultimo y la posicion donde
// debe insertar el codigo en el vector codificado (posicionBit).
// Se encarga de convertir los datos de codigo a bits e insertarlos en la posicion
// que les corresponde en datosCodificados. Longitud es la longitud de cada c�digo, posicionBit es la posici�n del bit en el que se va
//a insertar, y nuevoTamanio es el tamanio del archivo comprimido.
// Deben desarrollar esta funcion en su totalidad.
void agregarAlArreglo(unsigned char datosCodificados[], unsigned char codigo, int longitud, int posicionBit, int nuevoTamanio)
{
 	__asm
 	{
		sub esp , 40
		mov eax , posicionBit
		mov ebx , 8
		div ebx
		mov [ebp - 8] , eax                                          ;byteActual = posicionBit/8
		mov [ebp - 12] , edx                                         ;bitsIzquierda = posicionBit % 8
		mov eax , [ebp - 8]
		mov eax , datosCodificados[eax]
		mov [ebp - 16] , eax                                         ; letra = datosCodificados[byteActual]
		mov eax , 255
		mov [ebp - 4] , eax                                          ; hexa = 0xfff
		mov eax , 8
		sub eax , longitud
		mov [ebp -24] , eax                                          ; corrimiento = 8 - longitud
		mov ebx, [ebp-4]
		mov cl , [ebp - 24]
		shr ebx , cl                                    ; hexa >> corrimiento
		mov [ebp-4], ebx
		mov eax , [ebp - 4]
		and codigo , al                                             ; codigo & hexa
		mov al , codigo
		mov [ebp -28] , al                                          ; temp = codigo 
		
		mov eax , 8
		sub eax , longitud
		inc eax
		cmp [ebp - 12] , eax		                                 ; bitsIzquierda<(8 - longitud + 1)
		jge else
		mov eax, 8
	    sub eax, [ebp-12]
		mov ebx, 255
		mov ecx , eax
		shl ebx, cl
		mov eax, [ebp-8]
		and datosCodificados[eax], 	ebx                              ; datosCodificados[byteActual] &= (0xff << (8-bitsIzquierda)
		mov eax, 8
		sub eax, longitud
		sub eax, [ebp-12]
		mov ecx , eax
		shl [ebp-28],cl
        mov eax, [ebp-8]
		mov ebx, [ebp-28]
        or datosCodificados[eax], ebx                                ; datosCodificados[byteActual] |= (temp << ((8-longitud) -bitsIzquierda
		jmp fin
        else:

        mov eax, 8
	    sub eax, [ebp-12]
		mov ebx, 255
		mov ecx , eax
		shl ebx, cl
		mov eax, [ebp-8]
		and datosCodificados[eax], 	ebx                              ; datosCodificados[byteActual] &= (0xff << (8-bitsIzquierda)
		mov eax, 8
		sub eax, longitud
		mov ebx , [ebp - 12]
		sub ebx , eax
		mov ecx , ebx
		shr [ebp - 28] , cl                                         ; datosCodificados[byteActual] |= (temp >> (bitsIzquierda - (8-bitsIzquierda))
		mov eax , 0
		mov ebx , [ebp - 8]
		inc ebx
		and datosCodificados[ebx] , eax                              ; datosCodificados[byteActual+1] &= 0000
		mov eax, 8
		sub eax, [ebp-12]
		mov ebx, longitud
		sub ebx, eax
		mov eax, 8
		sub eax, ebx
		mov ecx , eax
		shl [ebp-28], cl
		mov eax, [ebp-8]
		inc eax
		mov ebx, [ebp-28]
		or datosCodificados[eax],ebx                                  ;  datosCodificados[byteActual+1] |= (temp <<(8-(longitud-(8-bitsIzquierda))))
		fin:
 	}

}

//-- Funcion main de la aplicacion
// No hay que completar nada en esta funcion.
int main(int argc, char *argv[])
{

	int tamanio;
	int tamanioCodificado;
	printf("%d \n", argc);
	if ( argc != 4 ){
		printf( "Faltan argumentos - Deben ser 3 archivos:\n" );
		printf( "  - archivo de entrada 1: nombre archivo a codificar (con el .txt)\n" );
        printf( "  - archivo con el c�digo de compresi�n (sin el .txt)\n" );
        printf( "  - archivo de salida: nombre del archivo de salida (con el .txt) \n" );
		system( "PAUSE" );
		return -1;
	}

	printf( "Archivo entrada %s\n", argv[1] );
	printf( "Codigo de compresion %s\n", argv[2] );
	printf( "Archivo de salida %s\n", argv[3] );

	Archivo * archivo = (Archivo *) malloc (sizeof (Archivo));
	Archivo * archivoCodificado = (Archivo *) malloc (sizeof (Archivo));


	tamanio = readFile(archivo, argv[1]);

	uploadCode(codigoCompresion, argv[2]);
	archivoCodificado -> informacion = ( unsigned char *) calloc (tamanio, sizeof( char));

	tamanioCodificado = codificar(archivo, archivoCodificado);
	//printf("%i\n", tamanioCodificado);
	writeFile((tamanioCodificado + 7) / 8, archivoCodificado, argv[3]);
	system("PAUSE");
}