#define _CRT_SECURE_NO_DEPRECATE 
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

//-- Constantes
#define TAMANIO_MAX 10000    //Tamanio maximo del archivo
#define NUMERO_CODIGOS 26      //Maximo de valores distintos en el archivo

//-- Definicion de la estructura para el archivo de entrada y el comprimido
typedef struct archivo
{
	int tamanio;
	unsigned char *informacion;
} Archivo;

//-- Arreglos que contendran el codigo de compresion
// arreglo que contiene el codigo. Cada byte contiene un codigo de compresion

unsigned char codigoCompresion[NUMERO_CODIGOS];
// arreglo con la longitud. Cada byte tiene la longitud en bits del codigo de compresion correspondiente 
// en el arreglo de arriba
int longitudCodigo[NUMERO_CODIGOS];



//-- Prototipos de las funciones
int readFile(Archivo * archivo, char *);
int readFileCode(unsigned char[], char *);
void writeFile(int, Archivo * archivoCodificado, char *);
int codificar(Archivo * archivo, Archivo * archivoCodificado);
void agregarAlArreglo(unsigned char[], unsigned char, int, int, int);
void uploadCode(unsigned char[], char *);


//-- Funciones

// Esta funcion se encarga de abrir un archivo y leerlo en el vector datos.
// Retorna el n�mero de bytes leidos.
// No hay que completar nada en esta funcion.
int readFile( Archivo * archivo, char * nombreArchivo )
{
	FILE *file;
	int n;
	int x;

	if (!(file = fopen(nombreArchivo, "rb"))) {
		printf("No se puede abrir el archivo: %s\n", nombreArchivo);
		exit(EXIT_FAILURE);
	}
	fseek(file, 0, SEEK_END);
	n = ftell(file);
	fseek(file, 0, SEEK_SET);
	
	archivo -> tamanio = n;	
	archivo -> informacion = (unsigned char *) calloc (n, sizeof (unsigned char));

	for ( x = 0; x < n; x++) {
			fread (&archivo -> informacion [x], sizeof(unsigned char ), 1, file);
		}
		
	fclose(file);
	
	return n;
}

// Esta funcion se encarga de abrir un archivo de codigo
// No hay que completar nada en esta funcion.
int readFileCode(unsigned char datos[], char *nombreArchivo)
{
	FILE *file;
	int n;

	if (!(file = fopen(nombreArchivo, "rb"))) {
		printf("No se puede abrir el archivo: %s\n", nombreArchivo);
		exit(EXIT_FAILURE);
	}
	fseek(file, 0, SEEK_END);
	n = ftell(file);
	fseek(file, 0, SEEK_SET);
	if (n > TAMANIO_MAX) {
		printf("Archivo demasiado grande: %s\n", nombreArchivo);
		exit(EXIT_FAILURE);
	}

	fread(datos, 1, n, file);

	fclose(file);

	return n;
}

// Esta funcion se encarga de abrir el archivo que contiene el codigo de compresion.
// Hace uso de la funcion readFile
// Guarda el codigo de compresion en el arreglo de estructuras llamado codigo
void uploadCode(unsigned char codigo[], char *nombreArchivoCodigo)
{
	unsigned char archivoCodigo[TAMANIO_MAX];
	//en la primera mitad guardamos el codigo de compresion, en la segunda mitad, en cada byte, 
	//guardamos la lognitud en bits de cada traduccion

	int tamanioCodigoTotal = readFileCode(archivoCodigo, nombreArchivoCodigo); // esto deberia ser 52
	int tamanioCodigo = tamanioCodigoTotal / 2; // deberia ser 26
	int i;
	
	printf("El tamanio del codigo es %d \n", tamanioCodigoTotal);

	//guardamos el codigo y su longitud en los arreglos de arriba
	for ( i = 0; i < tamanioCodigo; i++) {
		codigo[i]   = archivoCodigo[i];
	}

	for ( i = tamanioCodigo; i < tamanioCodigoTotal; i++) {
		longitudCodigo[i - tamanioCodigo] = (int)archivoCodigo[i];
	}
}


// Esta funcion se encarga de escribir un archivo a partir del vector datos.
// El numero de bytes que se deben escribir viene en el parametro n.
// No hay que completar nada en esta funcion.
void writeFile(int n, Archivo * archivoCodificado, char *nombreArchivo)
{
	FILE *file;
	if (!(file = fopen(nombreArchivo, "wb"))) {
		printf("No se puede abrir el archivo: %s\n", nombreArchivo);
		exit(EXIT_FAILURE);
	}

	fwrite((archivoCodificado->informacion), 1, n, file);

	fclose(file);
}


// Esta funcion se encarga de codificar cada uno de los valores que se encuentran en la estructura archivo y asignar los nuevos valores a la estructura archivocodificado.
// Deben desarrollar esta funcion en su totalidad.
int codificar(Archivo * archivo, Archivo * archivocodificado)
{
	int longitud;
	int ultimoBit = 0; // bit del arreglo de compresion en el que estamos
	unsigned char codigoa;
	int tamBytes = 0;
	int byte;
	int index;
	int numLetra;
	int n = archivo->tamanio;
	unsigned char * datos = archivo -> informacion;
	unsigned char * codificados = archivocodificado -> informacion;
	int copy = 0;
	
	for (index = 0; index < n; index++)
	{
		numLetra = datos[index] - 65;
		longitud = longitudCodigo[numLetra];
		copy += longitud; // va aumentando el ultimo bit
	}

	for (byte = 0; byte < n; byte++)
	{
		numLetra = datos[byte] - 65;
		codigoa = codigoCompresion[numLetra];
		longitud = longitudCodigo[numLetra];	
		agregarAlArreglo(codificados, codigoa, longitud, ultimoBit, (copy+7)/8);
		ultimoBit += longitud; // va aumentando el ultimo bit
	}
  
	archivocodificado->tamanio = (copy+7)/8;
	printf("Longitud %i\n", longitud);
	printf("ultimoBit %i\n ", ultimoBit);
	printf("codigoa %u\n ", codigoa);
	printf("tamBytes %i\n ", tamBytes);
	printf("byte %i\n ", byte);
	printf("index %i\n ", index);
	printf("numLetra %i\n ", numLetra);
	printf("n %i\n ", n);
	printf("codificados-datos %u\n ", codificados-datos);
	printf("copy %i\n ", copy);

	return ultimoBit;

}

// Esta funcion recibe como parametros el vector de datos codificados,
// el codigo que se debe insertar, la longitud de este ultimo y la posicion donde
// debe insertar el codigo en el vector codificado (posicionBit).
// Se encarga de convertir los datos de codigo a bits e insertarlos en la posicion
// que les corresponde en datosCodificados. Longitud es la longitud de cada código, posicionBit es la posición del bit en el que se va
//a insertar, y nuevoTamanio es el tamanio del archivo comprimido.
// Deben desarrollar esta funcion en su totalidad.
void agregarAlArreglo(unsigned char datosCodificados[], unsigned char codigo, int longitud, int posicionBit, int nuevoTamanio)
{

	int hexa;
	int byteactual = posicionBit / 8;
	int bitsIzquierda = posicionBit % 8;
	unsigned char letra = datosCodificados[byteactual];
	unsigned char tmp;
	//Para saber cuantos bits estan en 0
	hexa = 0xff;
	int corrimiento = 8 - longitud;
	hexa = hexa >> corrimiento;
	codigo &= hexa;
    unsigned char temp = codigo;


	if (bitsIzquierda<(8 - longitud + 1))//si esta contenido en 1 solo byte
	{
		datosCodificados[byteactual] &= (0xff << (8-bitsIzquierda));
		datosCodificados[byteactual] |= (temp << ((8 - longitud) - bitsIzquierda));
	}

	else //Si la subcadena esta entre 2 bytes
	{
		    datosCodificados[byteactual] &= ( 0xff << (8-bitsIzquierda));
			datosCodificados[byteactual] |= (temp >> (bitsIzquierda - (8 - longitud)));
			datosCodificados[byteactual + 1] &= (0x00);
			datosCodificados[byteactual + 1] |= (temp << ((8 - (longitud - (8 - bitsIzquierda)))));
	}

}

//-- Funcion main de la aplicacion
// No hay que completar nada en esta funcion.
int main()
{
	int tamanio;
	int tamanioCodificado;
	unsigned char nombreArchivo[] = "";
	unsigned char nombreCodigo[] = "";
	unsigned char nombreCodificado[] = "";
	Archivo * archivo = (Archivo *) malloc (sizeof (Archivo));
	Archivo * archivoCodificado = (Archivo *) malloc (sizeof (Archivo));
    unsigned char prueba[] = "";

	printf("Ingrese el nombre del archivo a comprimir (incluya el .txt): \n");
	scanf("%s", &nombreArchivo);
	tamanio = readFile(archivo, nombreArchivo);
	printf("El tamanio del archivo es %d \n", tamanio);
	printf("Ingrese el nombre del archivo que contiene el codigo (sin la extension de archivo): \n");
	scanf("%s", &nombreCodigo);
	uploadCode(codigoCompresion, nombreCodigo);
	archivoCodificado -> informacion = (unsigned char *) calloc (tamanio, sizeof(unsigned char));
	printf("Ingrese el nombre del archivo para guardar el archivo codificado (incluya el .txt): \n");
	scanf("%s", &nombreCodificado);
	tamanioCodificado = codificar(archivo, archivoCodificado);
	writeFile((tamanioCodificado + 7) / 8, archivoCodificado, nombreCodificado);

	system("PAUSE");
}
